import *as path from "path";
const {
    dbprod,
    dbtest,
    pwd,
    env,
    dbuser_prod
}=require(path.resolve('config.json'));

let dbconfig:{ URL:string,USERNAME:string,PASSWORD:string }=null;

switch (env) {
    case 'prod':
        dbconfig={
            URL:dbprod,
            USERNAME:dbuser_prod,
            PASSWORD:pwd
        }
        break;
    default:
        dbconfig={
            URL:dbtest,
            USERNAME:dbuser_prod,
            PASSWORD:pwd
        }
        break;
}


export default dbconfig;