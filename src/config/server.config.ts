import *as path from "path";
const {
    env,
    test_port,
    prod_port
}=require(path.resolve('config.json'));

export const _server = {
    port: 4040,
    host: 'localhost'
}

let config:{ host:string,port:string }=null;

switch (env) {
    case 'prod':
        config={
            port:prod_port,
            host:'localhost'
        }
        break;
    default:
        config={
            port:test_port,
            host:'localhost'
        }
        break;
}


export default config;