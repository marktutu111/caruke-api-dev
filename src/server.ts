'use strict';

const Hapi= require('@hapi/hapi');
import * as Good from "good";
import * as mongoose from "mongoose";

import { goodConfig } from "./config/good.config";
import serverConfig from "./config/server.config";
import * as path from "path";

// ROUTES //
import db from "./config/database.config";

/*app*/
import app from "./app/app";
import JWT from "./app/hapi-auth-jwt/jwt.auth";
import Modules from "./modules/module";

import Nsano from "./nsano";
import Cron from "./cron/cron";
import otp from "./otp/otp.api";


// Start the server
const init=async()=>{

    const server=Hapi.server(
        {
            port:serverConfig.port,
            host:serverConfig.host,
            routes: {
                cors: { 
                    origin: ['*']
                },
                files: {
                    relativeTo: path.resolve('public')
                }
            }
        }
    );

    // REGISTER SERVER PLUGINS //
    await server.register(
        [
            {
                plugin: Good,
                options: goodConfig
            },
            {
                plugin:JWT
            },
            {
                plugin:Modules
            },
            {
                plugin:Cron
            },
            {
                plugin:otp,
                routes:{
                    prefix:'/caruke/otp'
                }
            },
            {
                plugin:Nsano,
                routes:{
                    prefix:'/caruke/nsano'
                }
            }
        ]
    )

    // REGISTER SERVER ROUTES HERE //
    await server.register(app);

    // CONNECT TO DATABASE
    await mongoose.connect(db.URL,{ auth:{ username:db.USERNAME,password:db.PASSWORD }, useNewUrlParser:true });

    
    // START SERVER //
    await server.start();
    console.log('Server running on %s', server.info.uri);



};



process.on('unhandledRejection', (err) =>{
    console.log(err);
    process.exit(1);
});



init();