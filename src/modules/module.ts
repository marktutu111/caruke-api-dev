import {  formatePhonenumber} from "./formate-phone-number/index";

const modules={
    pkg:{
        name:'modules'
    },
    register:async(server)=>{
        server.method('formatNumber',formatePhonenumber);
    }
}

export default modules;