import * as moment from "moment";

const check=async(server)=>{
    try {
        const subscriptions=await server.methods.dbSubscription.find(
            {
                status:'active'
            }
        );
        subscriptions.forEach(async subscription=>{
            try {
                const {endDate,_id}=subscription;
                const daysRemaining=moment(endDate).diff(new Date(),'days');
                let status:'active'|'expired'=daysRemaining<=0?'expired':'active';
                await server.methods.dbSubscription.findOneAndUpdate(
                    {
                        _id:_id
                    },
                    {
                        $set:{
                            daysRemaining:daysRemaining,
                            status:status,
                            updatedAt:Date.now()
                        }
                    }
                );
            } catch (err) {}
        })
    } catch (err) {console.log(err)}
}


export default check;