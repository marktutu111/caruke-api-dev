import subscription from "./subscriptions.cron";
import *as cron from "cron";
const api={
    pkg:{
        name:'cron',
        version:'1'
    },
    register:async(server)=>{
        var job=new cron.CronJob(
            '0 0 * * *',
            ()=>subscription(server)
        );
        job.start();
    }
}


export default api;