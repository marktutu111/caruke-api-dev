import * as randomize from "randomatic";
import { formatePhonenumber } from "../../modules/formate-phone-number";

export const processOtp=(request,account:string)=>new Promise(async (resolve,reject)=>{
    try {
        const otp=randomize('0',5);
        const result=await request.server.methods.dbOtp.findOneAndUpdate(
            {
                account:account
            },
            {
                $set:{
                    otp:otp
                }
            },
            { upsert:true,new:true}
        );
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient:formatePhonenumber(account),
                    message: 'Your OTP is ' + otp
                }
            }
        ).catch(err=>null);
        resolve(result);
    } catch (err) {
        reject(err);
    }
});