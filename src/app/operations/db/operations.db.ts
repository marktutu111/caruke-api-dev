import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        service:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'services'
        },
        amount:{
            type:Number,
            default:0
        },
        description:String,
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
);

export default mongoose.model('operations',schema);