import schema from "./db/operations.db";
import route from "./route/operations.route";

const api={
    pkg:{
        name:'opxns',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbOperations',schema);
        server.route(route);
    }
}


export default api;