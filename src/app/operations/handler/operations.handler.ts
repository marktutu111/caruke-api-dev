
const addOpxns=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const data=request.payload;
        const results=await request.server.methods.dbOperations.create(
            {
                ...data,
                bay:bayId
            }
        );
        if(!results || !results._id){
            throw Error(
                'results created failed'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getbysvs=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const {id}=request.params;
        const result=await request.server.methods.dbOperations.find(
            {
                bay:bayId,
                service:id
            }
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const updateOpxns=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const {id,data}=request.payload;
        const results=await request.server.methods.dbOperations.findOneAndUpdate(
            {
                _id:id,
                bay:bayId
            },
            {
                $set:{...data,updatedAt:Date.now()}
            },{new:true}
        ).sort({createdAt:-1})
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const deleteOpxn=async(request,h)=>{
    try {
        const {id}=request.params;
        const result=await request.server.methods.dbOperations.findOneAndRemove(
            {
                _id:id
            }
        );
        if(!result || !result._id){
            throw Error(
                'Delete failed,something went wrong'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const fetchOpxns=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const results=await request.server.methods.dbOperations.find(
            {
                bay:bayId
            }
        ).sort({createdAt:-1}).populate('service');
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    addOpxns,
    updateOpxns,
    fetchOpxns,
    deleteOpxn,
    getbysvs
}