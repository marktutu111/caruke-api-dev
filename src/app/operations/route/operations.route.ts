import * as joi from "@hapi/joi";
import Handler from "../handler/operations.handler";

const route=[
    {
        path:'/get',
        method:'GET',
        handler:Handler.fetchOpxns
    },
    {
        path:'/add',
        method:'POST',
        config:{
            validate:{
                payload:joi.object(
                    {
                        service:joi.string().required(),
                        amount:joi.alternatives(
                            joi.string(),
                            joi.number()
                        ).required(),
                        description:joi.string().required()
                    }
                )
            }
        },
        handler:Handler.addOpxns
    },
    {
        path:'/update',
        method:'PUT',
        config:{
            validate:{
                payload:joi.object(
                    {
                        id:joi.string().required(),
                        data:joi.object().required()
                    }
                )
            }
        },
        handler:Handler.updateOpxns
    },
    {
        path:'/delete/{id}',
        method:'DELETE',
        config:{
            validate:{
                params:joi.object(
                    {
                        id:joi.string().required()
                    }
                )
            }
        },
        handler:Handler.deleteOpxn
    },
    {
        path:'/service/get/{id}',
        method:'GET',
        config:{
            validate:{
                params:joi.object(
                    {
                        id:joi.string().required()
                    }
                )
            }
        },
        handler:Handler.getbysvs
    }
]


export default route;