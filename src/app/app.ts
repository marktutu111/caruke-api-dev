import operations from "./operations/operations.api";
import services from "./services/services.api";
import sales from "./sales/sales.api";
import customers from "./customers/customers.api";
import washers from "./washers/washers.api";
import users from "./users/users.api";
import settings from "./settings/settings.api";
import invoice from "./invoice/invoice.api";
import subs from "./subscriptions/subscription.api";
import bay from "./bay/bay.api";
import transactions from "./transactions/transaction.api";
import expenses from "./expenses/expenses.api";
import sms from "./sms/sms.api";



const app={
    pkg:{
        name:'app',
        version:'1'
    },
    register:async(server)=>{
        server.register(
            [
                {
                    plugin:operations,
                    routes:{
                        prefix:'/caruke/operations'
                    }
                },
                {
                    plugin:invoice,
                    routes:{
                        prefix:'/caruke/invoice'
                    }
                },
                {
                    plugin:expenses,
                    routes:{
                        prefix:'/caruke/expenses'
                    }
                },
                {
                    plugin:services,
                    routes:{
                        prefix:'/caruke/services'
                    }
                },
                {
                    plugin:sales,
                    routes:{
                        prefix:'/caruke/sales'
                    }
                },
                {
                    plugin:customers,
                    routes:{
                        prefix:'/caruke/customers'
                    }
                },
                {
                    plugin:washers,
                    routes:{
                        prefix:'/caruke/washers'
                    }
                },
                {
                    plugin:users,
                    routes:{
                        prefix:'/caruke/users'
                    }
                },
                {
                    plugin:settings,
                    routes:{
                        prefix:'/caruke/settings'
                    }
                },
                {
                    plugin:subs,
                    routes:{
                        prefix:'/caruke/subscription'
                    }
                },
                {
                    plugin:transactions,
                    routes:{
                        prefix:'/caruke/transactions'
                    }
                },
                {
                    plugin:bay,
                    routes:{
                        prefix:'/caruke/bay'
                    }
                },
                {
                    plugin:sms,
                    routes:{
                        prefix:'/caruke/sms'
                    }
                }
            ]
        )
    }
}

export default app;