import db from "./db/sms.db";
import route from "./routes/sms.route";

const api={
    name:'sms',
    version:'1',
    register:async(server:any)=>{
        server.method('dbSms',db);
        server.route(route);
    }
}

export default api;