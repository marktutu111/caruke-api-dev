import { ServerRoute } from "@hapi/hapi";
import *as joi from "joi";
import Handler from "../handler/sms.handler";

const route:ServerRoute[]=[
    {
        path:'/new',
        method:'POST',
        options:{
            validate:{
                payload:joi.object(
                    {
                        bay:joi.string().required(),
                        amount:joi.alternatives(
                            joi.string(),
                            joi.number()
                        ).required(),
                        count:joi.alternatives(
                            joi.string().required(),
                            joi.number()
                        ).required(),
                    }
                )
            }
        },
        handler:Handler.newSms
    },
    {
        path:'/bay/get',
        method:'GET',
        handler:Handler.getBay
    },
    {
        path:'/get',
        method:'GET',
        handler:Handler.getAll
    }
]

export default route;