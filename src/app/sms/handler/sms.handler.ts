import { ResponseToolkit } from "@hapi/hapi";
import *as mongoose from "mongoose";

const newSms=async(request:any,h:ResponseToolkit)=>{
    try {
        const payload=request.payload;
        const result=await request.server.methods.dbSms.create(payload);
        if(!result || !result._id){
            throw Error(
                'Operation failed'
            )
        };
        return h.response(
            {
                success:true,
                message:'Sms topup successfull',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getAll=async(request,h:ResponseToolkit)=>{
    try {
        const result=await request.server.methods.dbSms.aggregate(
            [
                {
                    $match:{}
                },
                { "$lookup": { from: 'bays', localField: 'bay', foreignField: '_id', as: 'bay' }},
                { $unwind:{ path:'$bay' } },
                {
                    $group:{
                        _id:null,
                        count:{$sum:1},
                        totalAmount:{$sum:"$amount"},
                        totalSms:{$sum:"$count"},
                        transactions:{ $push:"$$ROOT" }
                    }
                }
            ]
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:result[0] || {}
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getBay=async(request:any,h:ResponseToolkit)=>{
    try {
        const {bayId}=request.auth.credentials;
        const result=await request.server.methods.dbSms.aggregate(
            [
                {
                    $match:{
                        bay:new mongoose.Types.ObjectId(bayId),
                    }
                },
                { "$lookup": { from: 'bays', localField: 'bay', foreignField: '_id', as: 'bay' }},
                { $unwind:{ path:'$bay' } },
                {
                    $group:{
                        _id:null,
                        count:{$sum:1},
                        totalAmount:{$sum:"$amount"},
                        totalSms:{$sum:"$count"},
                        transactions:{ $push:"$$ROOT" }
                    }
                }
            ]
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:result[0] || {}
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



export default {
    newSms,
    getAll,
    getBay
}