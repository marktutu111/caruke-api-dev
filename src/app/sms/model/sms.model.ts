
export interface Sms {
    bay:string;
    amount:number;
    count:number;
    createdAt:string;
    updatedAt:string;
}