import * as bcrypt from "bcrypt";
import *as path from "path";
const {secrete}=require(path.resolve('config.json'));


const JWT={
    pkg: {
        name:'jwt-auth',
        version: '1'
    },
    register:async(server)=>{
        try {
            const validate=async(decoded,request)=>{
                try {
                    const {
                        bayId,
                        account,
                        email
                    }=decoded;
                    let query={};
                    switch (account) {
                        case 'super':
                            query={
                                email:email,
                                status:'active'
                            }
                            break;
                        default:
                            query={
                                bay:bayId,
                                status:'active'
                            }
                            break;
                    }
                    const user=await request.server.methods.dbUsers.findOneAndUpdate(query,{$set:{lastSeen:Date.now()}});
                    if (!user || !user._id){
                        throw Error(
                            'user not found'
                        )
                    }
                    return {
                        isValid:true
                    }
                } catch (err) {
                    return { 
                        isValid:false 
                    }; 
                }
                
            };
            await server.register(require('hapi-auth-jwt2'));
            server.auth.strategy('jwt','jwt',
                { 
                    key:secrete, // Never Share your secret key
                    validate:validate, // validate function defined above
                    verifyOptions:{ algorithms: [ 'HS256' ] } // pick a strong algorithm
                }
            );

            server.auth.default('jwt');
            
        } catch (err) {
            console.log(err);
        }

    }
}


export default JWT;