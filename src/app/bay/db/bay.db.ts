import * as mongoose from "mongoose";


const schema=new mongoose.Schema(
    {
        name:String,
        phone:String,
        location:String,
        address:String,
        email:String,
        status:{
            type:String,
            enum:['blocked','active'],
            default:'active'
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export default mongoose.model('bays',schema);