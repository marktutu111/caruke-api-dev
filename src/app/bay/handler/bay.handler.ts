import { formatePhonenumber } from "../../../modules/formate-phone-number";

const add=async(request,h)=>{
    try {
        const data=request.payload;
        const _bay=await request.server.methods.dbBays.findOne(
            {
                email:data['email']
            }
        );
        if(_bay && _bay._id){
            throw Error(
                'you cannot register a bay with the same email twice'
            )
        }
        const bay=await request.server.methods.dbBays.create(
            data
        );
        if(!bay || !bay._id){
            throw Error(
                'failed'
            )
        };
        const _user={
            "bay":bay._id,
            "account":"bay",
            "fname":bay.name,
            "lname":"Admin",
            "email":bay.email,
            "password":"12345",
            "phone":bay.phone,
            "gender":"male",
            "role":["sales","addsale","services","washers","customers","subscriptions","users","invoices","expenses"],
            "status":"active"
        };
        const result=await request.server.methods.dbUsers.create(_user);
        if(!result || !result._id){
            throw Error(
                'Oops something went wrong,we could not register the bay on Carukee'
            )
        };
        request.server.methods.nsanoSendSMS(
            {
                type:'single',
                data:{
                    recipient:formatePhonenumber(bay.phone),
                    message:bay.name + '\n'
                        + 'your carukee account details:'
                        + 'email: '+bay.email+' '+'password:12345'
                        + '\n'
                        +'welcome to Carukee.'
                }
            }
        ).catch(err=>null);
        return h.response(
            {
                success:true,
                message:'Washing Bay created successfully,Login details sent to account phone number with default password',
                data:bay
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const update=async(request,h)=>{
    try {
        const {id,data}=request.payload;
        const bay=await request.server.methods.dbBays.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:{
                    ...data,
                    updatedAt:Date.now()
                }
            },{ new:true }
        );
        if(!bay || !bay._id){
            throw Error(
                'Bay not updated'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:bay
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getbays=async(request,h)=>{
    try {
        const results=await request.server.methods.dbBays.find(
            {}
        ).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    getbays,
    add,
    update
}