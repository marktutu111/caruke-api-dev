import * as joi from "joi";
import Handler from "../handler/bay.handler";

const route=[
    {
        path:'/add',
        method:'POST',
        config:{
            validate:{
                payload:joi.object(
                    {
                        name:joi.string().required(),
                        email:joi.string().required(),
                        phone:joi.string().required(),
                        location:joi.string().required(),
                        address:joi.string().required(),
                        status:joi.string().required()
                    }
                )
            }
        },
        handler:Handler.add
    },
    {
        path:'/get',
        method:'GET',
        handler:Handler.getbays
    },
    {
        path:'/update',
        method:'PUT',
        config:{
            validate:{
                payload:joi.object(
                    {
                        id:joi.string().required(),
                        data:joi.object()
                    }
                )
            }
        },
        handler:Handler.update
    }
]


export default route;