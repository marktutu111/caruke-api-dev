import db from "./db/bay.db";
import route from "./route/bay.route";

const api={
    pkg:{
        name:'bays',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbBays',db);
        server.route(route);
    }
}


export default api;