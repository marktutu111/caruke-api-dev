import db from "./db/settings.db";
import route from "./routes/settings.route";

const api={
    pkg:{
        name:'settings',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbSettings',db);
        server.route(route);
    }
}


export default api;