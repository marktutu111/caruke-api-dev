
const add=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const data=request.payload;
        const result=await request.server.methods.dbSettings.findOneAndUpdate(
            {
                bay:bayId
            },
            {
                $set:{
                    ...data,
                    updatedAt:Date.now()
                }
            },{new:true,upsert:true}
        )
        if(!result || !result._id){
            throw Error(
                'Sorry something went wrong,settings could not be updated'
            )
        };
        return h.response(
            {
                success:true,
                message:'Profile update successfully',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getbay=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const result=await request.server.methods.dbSettings.findOne(
            {
                bay:bayId
            }
        );
        if(!result || !result._id){
            throw Error(
                'not found'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    add,
    getbay
}