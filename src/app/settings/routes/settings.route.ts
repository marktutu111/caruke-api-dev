import * as joi from "joi";
import Handler from "../handler/settings.handler";

const route=[
    {
        path:'/add',
        method:'POST',
        config:{
            validate:{
                payload:joi.object(
                    {
                        sms_message:joi.string().required(),
                        sms_sales_toggle:joi.boolean().required()
                    }
                )
            }
        },
        handler:Handler.add
    },
    {
        path:'/get',
        method:'GET',
        handler:Handler.getbay
    }
]


export default route;