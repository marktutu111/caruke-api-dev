import *as mongoose from "mongoose";

const add=async(request,h)=>{
    try {
        const data=request.payload;
        const result=await request.server.methods.dbInvoice.create(data);
        if(!result || !result._id){
            throw Error(
                'Invoice generation failed'
            )
        };
        if(data.status==='paid'){
            await Promise.all(
                [
                    result.update(
                        {
                            datePaid:Date.now()
                        }
                    ),
                    request.server.methods.dbSales.updateMany(
                        {
                            _id:{ $in:data.sales }
                        },
                        {
                            $set:{
                                status:'paid',
                                updatedAt:Date.now()
                            }
                        }
                    )
                ]
            )
        }
        return h.response(
            {
                success:true,
                message:'Invoice has been recorded successfully',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const pay=async(request,h)=>{
    try {
        const {id}=request.payload;
        const invoice=await request.server.methods.dbInvoice.findOneAndUpdate(
            {
                _id:id,
                status:'pending'
            },
            {
                $set:{
                    status:'paid',
                    datePaid:Date.now(),
                    updatedAt:Date.now()
                }
            }
        );
        if(!invoice || !invoice._id){
            throw Error(
                'Invoice has already been processed'
            )
        };
        await request.server.methods.dbSales.updateMany(
            {
                _id:{ $in:invoice.sales }
            },
            {
                $set:{
                    status:'paid',
                    updatedAt:Date.now()
                }
            }
        );
        return h.response(
            {
                success:true,
                message:'Invoice processed successfully',
                data:invoice
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const getall=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const result=await request.server.methods.dbInvoice.aggregate(
            [
                {
                    $match:{
                        bay:new mongoose.Types.ObjectId(bayId)
                    }
                },
                { "$lookup": { from: 'bays', localField: 'bay', foreignField: '_id', as: 'bay' }},
                { "$lookup": { from: 'users', localField: 'user', foreignField: '_id', as: 'user' }},
                { "$lookup": 
                    { 
                        from:'sales',
                        let:{ sales: '$sales' },
                        pipeline:[
                            { $match:{ $expr:{ $in:['$_id','$$sales'] } } },
                            { $lookup:
                                {
                                    from:'services',
                                    localField:'service',
                                    foreignField: '_id', 
                                    as: 'service'
                                } 
                            },
                            {
                                $unwind:{ path:'$service' }
                            },
                            { $lookup:
                                {
                                    from:'operations',
                                    localField:'operations',
                                    foreignField: '_id', 
                                    as: 'operations'
                                }
                            }
                        ],
                        as:'sales' 
                    }
                },
                { $unwind:{ path:'$bay'}},
                { $unwind:{ path:'$user'}},
                { $sort:{ createdAt:-1 } },
                {
                    $group:{
                        _id:null,
                        "total":{$sum:"$total"},
                        "count":{$sum:1},
                        "items":{$push:"$$ROOT"}
                    }
                }
            ]
        ).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message:'success',
                data:result[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    add,
    getall,
    pay
}