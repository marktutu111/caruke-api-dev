import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        user:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'users'
        },
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        customerName:String,
        customerPhone:String,
        customerAddress:String,
        total:Number,
        vat:Number,
        subtotal:Number,
        status:{
            type:String,
            enum:[
                'pending',
                'paid'
            ]
        },
        sales:{
            type:[mongoose.Schema.Types.ObjectId],
            ref:'sales'
        },
        datePaid:Date,
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export default mongoose.model('invoices',schema);