import db from "./db/invoice.db";
import route from "./route/invoice.route";

const api={
    pkg:{
        name:'invoice',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbInvoice',db);
        server.route(route);
    }
}


export default api;