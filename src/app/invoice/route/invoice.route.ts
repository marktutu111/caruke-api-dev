import *as joi from "joi";
import Handler from "../handler/invoice.handler";


const route=[
    {
        path:'/add',
        method:'POST',
        config:{
            validate:{
                payload:joi.object(
                    {
                        user:joi.string().required(),
                        bay:joi.string().required(),
                        customerName:joi.string().required(),
                        customerAddress:joi.string().allow(''),
                        customerPhone:joi.string().required(),
                        total:joi.alternatives(
                            joi.number(),
                            joi.string()
                        ).required(),
                        vat:joi.alternatives(
                            joi.number(),
                            joi.string()
                        ).required(),
                        subtotal:joi.alternatives(
                            joi.number(),
                            joi.string()
                        ).required(),
                        status:joi.string().required(),
                        sales:joi.array(),
                    }
                )
            }
        },
        handler:Handler.add
    },
    {
        path:'/get',
        method:'GET',
        handler:Handler.getall
    },
    {
        path:'/pay',
        method:'POST',
        config:{
            validate:{
                payload:joi.object(
                    {
                        id:joi.string().required()
                    }
                )
            }
        },
        handler:Handler.pay
    }
]


export default route;