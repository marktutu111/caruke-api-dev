import * as request from "request";
import * as path from "path";
const config=require(path.resolve('config.json'));


const token=(bay:string)=>new Promise((resolve,reject)=>{
    const options={
        url:`${config.admin}/subscription/active/${bay}`
    };
    request.get(options.url,(err,res,bd)=>{
        try {
            if(err){
                throw Error(
                    err
                )
            }
            resolve(JSON.parse(bd));
        } catch (err) {
            reject(
                err
            )
        }
    })
})


export default token;