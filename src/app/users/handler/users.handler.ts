import *as mongoose from "mongoose";
import * as jsonwebtoken from "jsonwebtoken";
import * as path from "path";
const {secrete,deploy}=require(path.resolve('config.json'));


const add=async(request,h)=>{
    try {
        const {phone,email}=request.payload;
        const user=await request.server.methods.dbUsers.findOne(
            {
                $or:[
                    {
                        email:email
                    },
                    {
                        phone:phone
                    }
                ]
            }
        );
        if(user && user._id){
            throw Error(
                'User with email or phone number is already registered in the system'
            )
        };
        const _user=await request.server.methods.dbUsers.create(request.payload);
        return h.response(
            {
                success:true,
                message:'success',
                data:_user
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const login=async(request,h)=>{
    try {
        const {email,password}=request.payload;
        const result=await request.server.methods.dbUsers.findOne(
            {
                email:email,
                password:password,
                status:'active'
            }
        ).populate('bay');
        if(!result || !result._id){
            throw Error(
                'User account not found'
            )
        }
        let user=result.toObject();
        switch (user.account) {
            case 'bay':
                const subscription=await request.server.methods.dbSubscription.findOne(
                    {
                        bay:user.bay._id,
                        status:'active'
                    }
                );
                
                if(!subscription || !subscription._id){
                    throw Error(
                        'No valid subscription found'
                    )
                };
                let _token:string='';
                switch (deploy) {
                    case 'online':
                        _token=subscription.token_online;
                        break;
                    default:
                        _token=subscription.token;
                        break;
                }
                user['subscriptionToken']=_token;
                user['daysRemaining']=subscription.daysRemaining || subscription.nod;
                return h.response(
                    {
                        success:true,
                        message:'success',
                        data:user
                    }
                )
            default:
                const token:string=jsonwebtoken.sign(
                    {
                        account:'super',
                        email:user.email,
                        phone:user.phone
                    },secrete, 
                    {
                        algorithm:'HS256', 
                        expiresIn:`1h`
                    }
                );
                return h.response(
                    {
                        success:true,
                        message:'success',
                        data:{...user,subscriptionToken:token}
                    }
                )
        }
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getone=async(request,h)=>{
    try {
        const {id}=request.params;
        const result=await request.server.methods.dbUsers.findOne(
            {
                _id:id
            }
        ).populate('bay');
        if(!result || !result._id){
            throw Error(
                'user not found'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err
            }
        )
    }
}


const updatePassword=async(request,h)=>{
    try {
        const {id,oldpassword,newpassword}=request.payload;
        const customer=await request.server.methods.dbUsers.findOne(
            {
                _id:id
            }
        );
        if(!customer || !customer._id){
            throw Error(
                'User with account not found'
            )
        };
        const {password}=customer;
        if(password!==oldpassword){
            throw Error(
                'Old password does not match'
            )
        };
        await customer.update(
            {
                password:newpassword
            }
        );
        return h.response(
            {
                success:true,
                message:'User password updated successfully',
                data:customer
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getall=async(request,h)=>{
    try {
        const _query=request.query || {};
        const {bayId,account}=request.auth.credentials;
        let query=account==='bay'?{bay:new mongoose.Types.ObjectId(bayId),..._query}:{};
        const result=await request.server.methods.dbUsers.aggregate(
            [
                { $match:query},
                { "$lookup": { from: 'bays', localField: 'bay', foreignField: '_id', as: 'bay' }},
                { $sort:{createdAt:-1} },
                {
                    "$group": {
                        "_id":null,
                        "count":{$sum:1},
                        "items": {$push: '$$ROOT'}
                    }
                }
            ]
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:result[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const update=async(request,h)=>{
    try {
        const {id,data}=request.payload;
        const user=await request.server.methods.dbUsers.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:{
                    ...data,
                    updatedAt:Date.now()
                }
            },{ new:true }
        );
        if(!user || !user._id){
            throw Error(
                'User cannot be updated'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:user
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



export default {
    add,
    update,
    getone,
    getall,
    login,
    updatePassword
}