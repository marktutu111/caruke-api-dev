import db from "./db/users.db";
import routes from "./route/users.route";
import getToken from "./handler/getToken";

const api={
    pkg:{
        name:'users',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbUsers',db);
        server.method('getToken',getToken);
        server.route(routes);
    }
}


export default api;