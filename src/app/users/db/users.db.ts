import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        account:{
            type:String,
            default:'bay',
            enum:['bay','super']
        },
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        fname:String,
        lname:String,
        email:String,
        password:String,
        phone:String,
        gender:String,
        lastSeen:Date,
        role:[String],
        status:{
            type:String,
            default:'active',
            enum:['blocked','active']
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export default mongoose.model('users',schema);