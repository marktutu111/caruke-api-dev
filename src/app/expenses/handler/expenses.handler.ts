import *as moment from "moment";
import *as mongoose from "mongoose";

const addExpense=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const data=request.payload;
        const result=await request.server.methods.dbExpenses.create({...data,bay:bayId});
        if(!result || !result._id){
            throw Error(
                'Expense not added'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:data
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getExpenses=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const results=await request.server.methods.dbExpenses.aggregate(
            [
                { $match:{bay:new mongoose.Types.ObjectId(bayId)}},
                { $sort:{ createdAt:-1 } },
                { "$lookup":{from:'expacnts',localField:'category', foreignField:'_id', as:'category'}},
                { $unwind:{ path:'$category', preserveNullAndEmptyArrays: true } },
                {
                    "$group": {
                        "_id":null, 
                        "total":{$sum:"$amount"},
                        "count":{$sum:1},
                        "transactions":{ $push:'$$ROOT' }
                    }
                }
            ]
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:results[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const addCategory=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const data=request.payload;
        const result=await request.server.methods.dbExpenseCategory.create({...data,bay:bayId});
        if(!result || !result._id){
            throw Error(
                'Category add failed'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        console.log(err);
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getCategories=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const results=await request.server.methods.dbExpenseCategory.find(
            {
                bay:bayId
            }
        ).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const deleteExpense=async(request,h)=>{
    try {
        const {id}=request.params;
        let result=await request.server.methods.dbExpenses.findOneAndRemove(
            {
                _id:id
            }
        );
        if(!result || !result._id){
            throw Error(
                'Oops expense delete failed'
            )
        };
        return h.response(
            {
                success:true,
                message:'Expense deleted successfully',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};



const filterExpenses=async(request,h)=>{
    try {
        const {start,end,account}=request.query;
        let _start=moment(new Date(start || Date.now())).startOf('day').toDate(),
            _end=moment(new Date(end || Date.now())).endOf('day').toDate(),
            query:any={
                createdAt:{
                    $gte:_start,
                    $lte:_end
                }
            };

        if(mongoose.Types.ObjectId.isValid(account)){
            query={
                ...query,
                category:new mongoose.Types.ObjectId(account)
            }
        }
        console.log(query);
        
        const result=await request.server.methods.dbExpenses.aggregate(
            [
                {
                    $match:query
                },
                { $sort:{ createdAt:-1 } },
                { "$lookup":{from:'expacnts',localField:'category', foreignField:'_id', as:'category'}},
                { $unwind:{ path:'$category', preserveNullAndEmptyArrays: true } },
                {
                    "$group": {
                        "_id":null, 
                        "total":{$sum:"$amount"},
                        "count":{$sum:1},
                        "transactions":{ $push:'$$ROOT' }
                    }
                }
            ]
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:result[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}




const deleteCategory=async(request,h)=>{
    try {
        const {id}=request.params;
        let result=await request.server.methods.dbExpenses.findOne(
            {
                category:id
            }
        )
        if(result && result._id){
            throw Error(
                'Expence account cannot be deleted because it is linked to an Expense record'
            )
        };
        result=await request.server.methods.dbExpenseCategory.findOneAndRemove(
            {
                _id:id
            }
        );
        if(!result || !result._id){
            throw Error(
                'Oops something went wrong, account not deleted'
            )
        }
        return h.response(
            {
                success:true,
                message:'Expense account deleted successfully',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

export default {
    addExpense,
    getExpenses,
    addCategory,
    getCategories,
    deleteCategory,
    deleteExpense,
    filterExpenses
}