import *as mongoose from "mongoose";


const expSchema=new mongoose.Schema(
    {
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        name:String,
        description:String,
        deleted:Boolean,
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


const schema=new mongoose.Schema(
    {
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        category:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'expacnts'
        },
        amount:Number,
        tax:Number,
        reference:String,
        expenseDate:Date,
        notes:String,
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


const expenses=mongoose.model('expenses',schema);
const category=mongoose.model('expacnts',expSchema);


export default {
    expenses,
    category
}