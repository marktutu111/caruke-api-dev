import schema from "./db/expenses.db";
import route from "./route/expenses.route";

const api={
    pkg:{
        name:'expenses',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbExpenses',schema.expenses);
        server.method('dbExpenseCategory',schema.category);
        server.route(route);
    }
}


export default api;