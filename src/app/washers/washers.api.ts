import db from "./db/washers.db";
import route from "./routes/washer.route";

const api={
    pkg:{
        name:'washer',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbWashers',db);
        server.route(route);
    }
}

export default api;