

const washer=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const {phone}=request.payload;
        const _washer=await request.server.methods.dbWashers.findOne(
            {
                phone:phone,
                bay:bayId
            }
        );
        if(_washer && _washer._id){
            throw Error(
                'Washer with phone number is already registered in the system'
            )
        }
        const result=await request.server.methods.dbWashers.create(
            {
                ...request.payload,
                bay:bayId
            }
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const update=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const {id,data}=request.payload;
        const result=await request.server.methods.dbWashers.findOneAndUpdate(
            {
                _id:id,
                bay:bayId
            },
            {
                $set:{
                    ...data,
                    updatedAt:Date.now()
                }
            },{new:true}
        );
        if(!result || !result._id){
            throw Error(
                'update failed'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const fetchall=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const results=await request.server.methods.dbWashers.find(
            {
                bay:bayId
            }
        ).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



export default {
    washer,
    update,
    fetchall
}