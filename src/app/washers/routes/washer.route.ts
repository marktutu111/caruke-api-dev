import * as joi from "joi";
import Handler from "../handler/washers.handler";

const routes=[
    {
        path:'/get',
        method:'GET',
        handler:Handler.fetchall
    },
    {
        path:'/add',
        method:'POST',
        config:{
            validate:{
                payload:joi.object(
                    {
                        fname:joi.string().required(),
                        lname:joi.string().required(),
                        phone:joi.string().required().max(10),
                        location:joi.string().required(),
                        gender:joi.string().required(),
                        status:joi.string().required().default('active')
                    }
                )
            }
        },
        handler:Handler.washer
    },
    {
        path:'/update',
        method:'PUT',
        config:{
            validate:{
                payload:joi.object(
                    {
                        id:joi.string().required(),
                        data:joi.object().required()
                    }
                )
            }
        },
        handler:Handler.update
    }
]


export default routes;