import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        fname:String,
        lname:String,
        phone:String,
        location:String,
        gender:String,
        status:{
            type:String,
            default:'active',
            enum:[
                'blocked',
                'active'
            ]
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export default mongoose.model('washers',schema);