import * as joi from "joi";
import Handler from "../handler/transactions.handler";

const route=[
    {
        path:'/get',
        method:'GET',
        handler:Handler.getall
    }
]


export default route;