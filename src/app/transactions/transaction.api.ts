import db from "./db/transaction.db";
import route from "./route/transaction.route";

const api={
    pkg:{
        name:'transaction',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbTransactions',db);
        server.route(route);
    }
}


export default api;