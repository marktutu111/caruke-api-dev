import { ResponseToolkit } from "@hapi/hapi";


const getall=async(request:any,h:ResponseToolkit)=>{
    try {
        const result=await request.server.methods.dbTransactions.find({});
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:true,
                message:err.message
            }
        )
    }
}


export default {
    getall
}