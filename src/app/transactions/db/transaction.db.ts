import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        subscription:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'subscriptions'
        },
        accountNumber:String,
        accountIssuer:String,
        amount:Number,
        status:{
            type:String,
            default:'pending',
            enum:[
                'pending',
                'failed',
                'paid'
            ]
        },
        callbackRef:String,
        transaction:Object,
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export default mongoose.model('transactions',schema);