import * as moment from "moment";
import *as mongoose from "mongoose";


const addsale=async(request,h)=>{
    try {

        const {bayId}=request.auth.credentials;
        
        const {
            customerName,
            customerPhone,
            paymentType,
            commission,
            total,
            sendSms
        }=request.payload;
        let customer=await request.server.methods.dbCustomers.findOne(
            {
                phone:customerPhone
            }
        );
        if(!customer || !customer._id){
            customer=await request.server.methods.dbCustomers.create(
                {
                    bay:bayId,
                    phone:customerPhone,
                    name:customerName,
                    blocked:false
                }
            );
        };
        const _id=customer._id;
        const _tno:string=`${_id.toString().substring(0,5)}${new Date().getDay()}`;
        let _status:string='';
        switch (paymentType) {
            case 'cash':
                _status='paid';
                break;
            case 'credit':
                _status='credit';
            default:
                _status='pending';
                break;
        }
        const _total=parseFloat(total);
        const _c=((parseFloat(commission)/100)*_total).toFixed(2);
        const _earning=_total-parseFloat(_c);
        const result=await request.server.methods.dbSales.create(
            {
                ...request.payload,
                customer:customer._id,
                status:_status,
                washerCommission:_c,
                profit:_earning,
                bay:bayId,
                trackingNumber:`CA${_tno.toUpperCase()}`
            }
        );
        if(!result || !result._id){
            throw Error(
                'Oops something went wrong.'
            )
        };
        if(sendSms){
            try {
                const settings=await request.server.methods.dbSettings.findOne(
                    {
                        bay:bayId
                    }
                );
                request.server.methods.sendSms(
                    {
                        "number":request.server.methods.formatNumber(customer.phone),
                        "message":'Hello '+customer.name
                            +'\n'
                            + settings.sms_message
                    }
                ).catch(err=>console.log('sms erorr'+err.message));
            } catch (err) {}
        }
        return h.response(
            {
                success:true,
                message:'transaction received and processed successfully',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};



const updateSale=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const {id,data}=request.payload;
        const result=await request.server.methods.dbSales.findOneAndUpdate(
            {
                _id:id,
                bay:bayId
            },
            {
                $set:{...data,updatedAt:Date.now()}
            },
            {
                new:true
            }
        );
        if(!result || !result._id){
            throw Error(
                'Sale could not be updated'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const payInvoice=async(request,h)=>{
    try {
        const {id}=request.payload;
        const result=await request.server.methods.dbSales.findOneAndUpdate(
            {
                _id:id,
                status:{
                    $ne:'paid'
                }
            },
            {
                $set:{
                    status:'paid',
                    updated:Date.now()
                }
            },{ new:true }
        );
        if(!result || !result._id){
            throw Error(
                'Invoice processing failed,this invoice has already been paid'
            )
        }
        return h.response(
            {
                success:true,
                message:'Invoice has been processed and recorded successfully',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const summary=async(request,h)=>{
    try {
        const {bayId,account}=request.auth.credentials;
        let query=account==='bay'?{bay:new mongoose.Types.ObjectId(bayId)}:{};
        const results=await request.server.methods.dbSales.aggregate(
            [
                {
                    $match:query
                },
                {
                    $facet:{
                        transactions:[
                            { "$lookup": { from: 'services', localField: 'service', foreignField: '_id', as: 'service' }},
                            { "$lookup": { from: 'customers', localField: 'customer', foreignField: '_id', as: 'customer' }},
                            { "$lookup": { from: 'operations', localField: 'operations', foreignField: '_id', as: 'operations' }},
                            { "$lookup": { from: 'washers', localField: 'washers', foreignField: '_id', as: 'washers' }},
                            { $limit:10 },
                            {
                                "$group": {
                                    "_id":null, 
                                    "items": {$push: '$$ROOT'}
                                }
                            }

                        ],
                        today:[
                            {
                                $match:{
                                    createdAt:{
                                        $gte:moment().startOf('day').toDate(),
                                        $lte:moment().endOf('day').toDate()
                                    }
                                }
                            },
                            { "$lookup": { from: 'services', localField: 'service', foreignField: '_id', as: 'service' }},
                            { "$lookup": { from: 'customers', localField: 'customer', foreignField: '_id', as: 'customer' }},
                            { "$lookup": { from: 'operations', localField: 'operations', foreignField: '_id', as: 'operations' }},
                            { "$lookup": { from: 'washers', localField: 'washers', foreignField: '_id', as: 'washers' }},
                            {
                                "$group": {
                                    "_id":null, 
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$washerCommission"},
                                    "count":{$sum:1}
                                }
                            }
                        ],
                        week:[
                            {
                                $match:{
                                    createdAt:{
                                        $gte:moment().startOf('week').toDate(),
                                        $lte:moment().endOf('week').toDate()
                                    }
                                }
                            },
                            {
                                "$group": {
                                    "_id":null, 
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$washerCommission"},
                                    "count":{$sum:1},
                                }
                            }
                        ],
                        month:[
                            {
                                $match:{
                                    createdAt:{
                                        $gte:moment().startOf('month').toDate(),
                                        $lte:moment().endOf('month').toDate()
                                    }
                                }
                            },
                            {
                                "$group": {
                                    "_id":null, 
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$washerCommission"},
                                    "count":{$sum:1},
                                }
                            }
                        ],
                        year:[
                            {
                                $match:{
                                    createdAt:{
                                        $gte:moment().startOf('year').toDate(),
                                        $lte:moment().endOf('year').toDate()
                                    }
                                }
                            },
                            {
                                "$group": {
                                    "_id":null, 
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$washerCommission"},
                                    "count":{$sum:1},
                                }
                            }
                        ],
                        services:[
                            { "$lookup": { from: 'services', localField: 'service', foreignField: '_id', as: 'service' }},
                            {
                                "$group": {
                                    "_id":"$service.name", 
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$washerCommission"},
                                    "count":{$sum:1}
                                }
                            },
                        ],
                        washers:[
                            { "$lookup": { from: 'washers', localField: 'washers', foreignField: '_id', as: 'washer' }},
                            {
                                "$group": {
                                    "_id":"$washer", 
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$washerCommission"},
                                    "count":{$sum:1}
                                }
                            },
                        ],
                        paymentType:[
                            {
                                "$group": {
                                    "_id":"$paymentType", 
                                    "sales":{$sum:"$total"},
                                    "count":{$sum:1}
                                }
                            },
                        ],
                        all:[
                            {
                                "$group": {
                                    "_id":null, 
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$washerCommission"},
                                    "count":{$sum:1}
                                }
                            },
                        ]
                    }
                }
            ]
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:results[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};


const dateFilter=async(request,h)=>{
    try {
        const {start,end,washer}=request.query;
        const {bayId}=request.auth.credentials;
        let query:any={
            bay:new mongoose.Types.ObjectId(bayId),
            createdAt:{
                $gte:moment(start).startOf('days').toDate(),
                $lte:moment(end).endOf('dates').toDate()
            }
        };
        switch (typeof washer==='string') {
            case true:
                query={...query,washers:new mongoose.Types.ObjectId(washer)}
                break;
            default:
                query=query;
                break;
        };
        const results=await request.server.methods.dbSales.aggregate(
            [
                {
                    $match:query
                },
                { "$lookup": { from: 'services', localField: 'service', foreignField: '_id', as: 'service' }},
                { "$lookup": { from: 'customers', localField: 'customer', foreignField: '_id', as: 'customer' }},
                { "$lookup": { from: 'operations', localField: 'operations', foreignField: '_id', as: 'operations' }},
                { "$lookup": { from: 'washers', localField: 'washers', foreignField: '_id', as: 'washers' }},
                { "$lookup": { from: 'users', localField: 'editor', foreignField: '_id', as: 'editor' }},
                { $unwind:{ path:'$service' } },
                { $unwind:{ path:'$customer' } },
                { $unwind:{ path:'$editor',preserveNullAndEmptyArrays: true} },
                { $sort:{ createdAt:-1 } },
                {
                    $facet:{
                        summary:[
                            {
                                "$group": {
                                    "_id":"$paymentType", 
                                    "sales":{$sum:"$total"},
                                    "count":{$sum:1},
                                }
                            }
                        ],
                        all:[
                            {
                                "$group": {
                                    "_id":null, 
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$washerCommission"},
                                    "count":{$sum:1},
                                    "items":{ $push:'$$ROOT' }
                                }
                            }
                        ]
                    }
                }
            ]
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:results[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const getSales=async(request,h)=>{
    try {
        const results=await request.server.methods.dbSales.aggregate(
            [
                {
                    $match:{}
                },
                { "$lookup": { from: 'services', localField: 'service', foreignField: '_id', as: 'service' }},
                { "$lookup": { from: 'customers', localField: 'customer', foreignField: '_id', as: 'customer' }},
                { "$lookup": { from: 'operations', localField: 'operations', foreignField: '_id', as: 'operations' }},
                { "$lookup": { from: 'washers', localField: 'washers', foreignField: '_id', as: 'washers' }},
                { $sort:{ createdAt:-1 } },
                {
                    $facet:{
                        summary:[
                            {
                                "$group": {
                                    _id:"$paymentType", 
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$commission"},
                                    "count":{$sum:1}
                                }
                            }
                        ],
                        items:[
                            {
                                "$group": { 
                                    _id:null,
                                    "profit":{$sum:"$profit"},
                                    "sales":{$sum:"$total"},
                                    "commissions":{$sum:"$commission"},
                                    "count":{$sum:1},
                                    "items":{ $push:'$$ROOT' }
                                }
                            }
                        ]
                    }
                }
            ]
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:results[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



export default {
    addsale,
    getSales,
    summary,
    dateFilter,
    payInvoice,
    updateSale
}