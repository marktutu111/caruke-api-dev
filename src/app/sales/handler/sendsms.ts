import *as request from "request";
import * as path from "path";
const config=require(path.resolve('config.json'));

const sendSms=(data)=>new Promise((resolve,reject)=>{
    const url=`${config.admin}/nsano/sendsms`
    request.post({url:url,headers:{ 'Content-type':'Application/json' },body:JSON.stringify(data)},(err,res,bd)=>{
        try {
            if(err){
                throw Error(
                    err
                )
            }
            resolve(JSON.parse(bd));
        } catch (err) {
            reject(bd);
        }
    })
})


export default sendSms;