import db from "./db/sales.db";
import route from "./routes/sales.route";
import sendsms from "./handler/sendsms";

const api={
    pkg:{
        name:'sales',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbSales',db);
        server.method('sendSms',sendsms);
        server.route(route);
    }
}

export default api;