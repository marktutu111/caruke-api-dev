import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        name:{
            type:String,
            index:'$text'
        },
        phone:{
            type:String,
            index:'text'
        },
        birthMonth:String,
        birthDay:String,
        blocked:{
            type:Boolean,
            default:false
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export default mongoose.model('customers',schema);