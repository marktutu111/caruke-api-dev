import * as joi from "joi";
import Handler from "../handler/customers.handler";

const route=[
    {
        path:'/get',
        method:'GET',
        config:{
            validate:{
                query:joi.object(
                    {
                        search:joi.string().optional()
                    }
                )
            }
        },
        handler:Handler.getall
    },
    {
        path:'/add',
        method:'POST',
        config:{
            validate:{
                payload:joi.object(
                    {
                        name:joi.string().required(),
                        phone:joi.string().required(),
                        birthMonth:joi.string().optional().allow(null),
                        birthDay:joi.string().optional().allow(null),
                    }
                )
            }
        },
        handler:Handler.add
    },
    {
        path:'/update',
        method:'PUT',
        config:{
            validate:{
                payload:joi.object(
                    {
                        id:joi.string().required(),
                        data:joi.object().required() 
                    }
                )
            }
        },
        handler:Handler.update
    },
    {
        path:'/delete/{id}',
        method:'DELETE',
        config:{
            validate:{
                params:joi.object(
                    {
                        id:joi.string()
                    }
                )
            }
        },
        handler:Handler.deleteHandle
    }
]


export default route;