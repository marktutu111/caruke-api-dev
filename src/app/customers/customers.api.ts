import db from "./db/customers.db";
import route from "./routes/customers.route";

const api={
    pkg:{
        name:'customers',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbCustomers',db);
        server.route(route);
    }
}

export default api;