import *as mongoose from "mongoose";

const add=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const {phone}=request.payload;
        const customer=await request.server.methods.dbCustomers.findOne(
            {
                phone:phone,
                bay:bayId
            }
        );
        if(customer && customer._id){
            throw Error(
                'Customer with phone number is already in the system'
            )
        }
        const result=await request.server.methods.dbCustomers.create(
            {
                ...request.payload,
                bay:bayId
            }
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};


const update=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const {id,data}=request.payload;
        const result=await request.server.methods.dbCustomers.findOneAndUpdate(
            {
                _id:id,
                bay:bayId
            },
            {$set:{...data,updatedAt:Date.now()}
            },{new:true}
        );
        if(!result || !result._id){
            throw Error(
                'update failed'
            )
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const deleteHandle=async(request,h)=>{
    try {
        const {id}=request.params;
        const result=await request.server.methods.dbCustomers.findOneAndRemove(
            {
                _id:id
            }
        );
        if(!result || !result._id){
            throw Error(
                'Delete failed'
            )
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getall=async(request,h)=>{
    try {
        const {search}=request.query;
        const {bayId}=request.auth.credentials;
        let query:any={bay:new mongoose.Types.ObjectId(bayId)};
        if(typeof search==='string'){query={...query,$text: { $search:search }}}
        const results=await request.server.methods.dbCustomers.aggregate(
            [
                {$match:query},
                {$sort:{createdAt:-1}},
                { "$lookup": { from: 'bays', localField: 'bay',foreignField: '_id', as: 'bay' }},
                {
                    $group:{
                        _id:null,
                        count:{ $sum:1 },
                        items:{ $push:'$$ROOT' }
                    }
                }
            ]
        );
        console.log(results);
        return h.response(
            {
                success:true,
                message:'success',
                data:results[0]
            }
        )
    } catch (err) {
        console.log(err);
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



export default {
    add,
    getall,
    update,
    deleteHandle
}