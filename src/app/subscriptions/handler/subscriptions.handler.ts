import { ResponseToolkit } from "@hapi/hapi";
import * as moment from "moment";

const add=async(request,h)=>{
    try {
        const {
            duration,
            durationType,
            bay,
            accountNumber,
            accountIssuer,
            otp
        }=request.payload;

        const verifyOtp=await request.server.methods.dbOtp.findOne(
            {
                account:accountNumber,
                otp:otp
            }
        )

        if (!verifyOtp || !verifyOtp['_id']) {
            throw Error(
                'OTP does not match, We cannot process your transaction'
            );
        }

        const cpd=10;
        const start=moment();
        const endDate=moment().add(durationType,duration);
        const _nod=start.diff(endDate,'days');
        const _tc=(cpd*_nod).toFixed(2);
        const _data={
            bay:bay,
            amount:_tc,
            startDate:start.toDate(),
            endDate:endDate.toDate(),
            code:`${bay.substring(0,6)}${_nod}${cpd}`,
            cpd:cpd,
            status:'pending'
        };
        const subscripition=await request.server.methods.dbSubscription.create(
            _data
        );
        if(!subscripition || !subscripition._id){
            throw Error(
                'Sorry we could not process your subscription request'
            )
        };
        const transaction=await request.server.methods.dbTransactions.create(
            {
                bay:bay,
                subscription:subscripition._id,
                accountNumber:accountNumber,
                accountIssuer:accountIssuer,
                amount:_tc
            }
        );
        const payload={
            kuwaita:'malipo',
            amount:transaction.amount.toString(),
            mno:accountIssuer,
            refID:transaction._id,
            msisdn:accountNumber
        }
        const response=await request.server.methods.nsanoPay(payload);
        await transaction.update(
            {
                transaction:response,
                callbackRef:response.reference
            }
        );
        let message:string='';
        switch (payload.mno) {
            case 'MTN':
                message="Transaction Pending. Kindly dial *170#, select 6) Wallet, Choose 3) My Approvals and  enter MM PIN to approve payment immediately."
                break;
            default:
                message="Your transaction is received, you will receive a prompt to authorize payment on your phone."
                break;
        }
        switch (response.code) {
            case '00':
                return h.response(
                    {
                        success: true,
                        message:message,
                        data:transaction
                    }
                )
            default:
                Promise.all(
                    [
                       request.server.methods.dbSubscription.findOneAndRemove(
                           {
                               _id:subscripition._id
                           }
                       ),
                       request.server.methods.dbTransactions.findOneAndRemove(
                            {
                                _id:transaction._id
                            }
                        )
                    ]
                )
                throw Error(response.msg);
        }
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const createSubscription=async(request,h)=>{
    try {
        const {
            duration,
            durationType,
            bay
        }=request.payload;
        const cpd=10;
        const start=moment();
        const endDate=moment().add(durationType,duration);
        const _nod=endDate.diff(start,'days');
        const _tc=(cpd*_nod).toFixed(2);
        const _data={
            bay:bay,
            amount:_tc,
            startDate:start.toDate(),
            endDate:endDate.toDate(),
            code:`${bay.substring(0,6)}${_nod}${cpd}`,
            cpd:cpd,
            nod:_nod,
            status:'pending'
        };
        const subscripition=await request.server.methods.dbSubscription.create(
            _data
        );
        if(!subscripition || !subscripition._id){
            throw Error(
                'Sorry we could not process your subscription request'
            )
        };
        const token=await request.server.methods.getSubToken(
            request,
            subscripition
        );
        return h.response(
            {
                success:true,
                message:'Token generated successfully',
                data:token
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const subscriptionSummary=async(request:any,h:ResponseToolkit)=>{
    try {
        const result=await request.server.methods.dbSubscription.aggregate(
            [
                {
                    $match:{}
                },
                { $lookup:{ from:"bays",localField: 'bay', foreignField: '_id', as: 'bay' } },
                { $unwind:{ path:"$bay",preserveNullAndEmptyArrays: true } },
                {
                    $group:{
                        _id:null
                    }
                }
            ]
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const getsubs=async(request,h)=>{
    try {
        const {id}=request.params;
        const query=typeof id==='string'?{bay:id}:{};
        const result=await request.server.methods.dbSubscription.find(
            query
        ).sort({createdAt:-1}).populate('bay');
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const getactive=async(request,h)=>{
    try {
        const {id}=request.params;
        const result=await request.server.methods.dbSubscription.findOne(
            {
                $or:[
                    {_id:id},
                    {bay:id}
                ],
                status:'active'
            }
        ).populate('bay');
        if(!result || !result._id){
            throw Error(
                'No subscription found'
            )
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getone=async(request,h)=>{
    try {
        const {id}=request.params;
        const result=await request.server.methods.dbSubscription.findOne(
            {
                $or:[
                    {
                        _id:id
                    },
                    {
                        bay:id
                    }
                ]
            }
        ).populate('bay');
        if(!result || !result._id){
            throw Error(
                'No subscription found'
            )
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}

const deleteSub=async(request,h)=>{
    try {
        const {id}=request.params;
        const subscription=await request.server.methods.dbSubscription.findOneAndRemove(
            {
                _id:id
            }
        );
        if(!subscription || !subscription._id){
            throw Error(
                'Subscription not found'
            )
        };
        return h.response(
            {
                success:false,
                message:'success',
                data:subscription
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



export default {
    add,
    getsubs,
    createSubscription,
    deleteSub,
    getone,
    getactive
}