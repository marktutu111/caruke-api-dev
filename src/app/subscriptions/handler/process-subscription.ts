import * as jsonwebtoken from "jsonwebtoken";
import * as path from "path";
const config=require(path.resolve('config.json'));



const subscribe=(request,subscription)=>new Promise(async(resolve,reject)=>{
    try {
        const _subscription=await request.server.methods.dbSubscription.findOne(
            {
                _id:subscription,
                status:'pending'
            }
        ).populate('bay');
        if(!_subscription || !_subscription._id){
            throw Error(
                'subscription not found'
            )
        };
        const {nod,bay}=_subscription;
        const token:string=jsonwebtoken.sign(
            {
                subscriptionId:subscription._id,
                bayId:bay._id,
                account:'bay'
            },_subscription.bay._id.toString(), 
            {
                algorithm:'HS256', 
                expiresIn:`${nod}d`
            }
        );
        const token_online:string=jsonwebtoken.sign(
            {
                subscriptionId:subscription._id,
                bayId:bay._id,
                account:'bay'
            },config.secrete, 
            {
                algorithm:'HS256', 
                expiresIn:`${nod}d`
            }
        );
        if(typeof token!=='string'){
            throw Error(
                'subscription token generation failed'
            )
        }
        await _subscription.update(
            {
                token:token,
                token_online:token_online,
                status:'active'
            }
        );
        resolve(token);
    } catch (err) {
        request.server.methods.dbSubscription.findOneAndRemove(
            {
                _id:subscription
            }
        ).catch(err=>null);
        reject(
            err
        )
    }
})


export default subscribe;