import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        amount:Number,
        startDate:Date,
        endDate:Date,
        code:String,
        token:String, // token offline
        token_online:String, // use this token when online
        nod:String, // number of days
        daysRemaining:String,
        cpd:Number, // charge per day
        status:{
            type:String,
            enum:[
                'expired',
                'active',
                'pending'
            ]
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export default mongoose.model('subscriptions',schema);