import db from "./db/subscriptions.db";
import route from "./route/subscription.route";
import subscribe from "./handler/process-subscription";

const api={
    pkg:{
        name:'subscription',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbSubscription',db);
        server.method('getSubToken',subscribe);
        server.route(route);
    }
}

export default api;