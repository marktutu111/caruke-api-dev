import * as mongoose from "mongoose";

const add=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const data=request.payload;
        const result=await request.server.methods.dbServices.create(
            {
                ...data,
                bay:bayId
            }
        );
        if(!result || !result._id){
            throw Error(
                'failed'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getall=async(request,h)=>{
    try {
        const {bayId}=request.auth.credentials;
        const results=await request.server.methods.dbServices.find(
            {
                bay:bayId
            }
        ).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const deleteInventory=async(request,h)=>{
    try {
        const {id}=request.params;
        const valid=mongoose.Types.ObjectId.isValid(id);
        if(!valid){
            throw Error(
                'Invalid ID provided'
            )
        }
        const result=await request.server.methods.dbServices.findOneAndRemove(
            {
                _id:id
            }
        );
        if(!result || !result._id){
            throw Error(
                'Delete failed'
            )
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    add,
    getall,
    deleteInventory
}