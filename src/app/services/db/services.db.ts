import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        bay:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'bays'
        },
        name:String,
        image:String,
        commission:{
            type:Number,
            default:0   
        },
        status:{
            type:String,
            enum:[
                'blocked',
                'active'
            ]
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export default mongoose.model('services',schema);