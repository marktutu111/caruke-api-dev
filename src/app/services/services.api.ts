import db from "./db/services.db";
import route from "./routes/services.route";

const api={
    pkg:{
        name:'services',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbServices',db);
        server.route(route);
    }
}

export default api;