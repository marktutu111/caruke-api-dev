import * as joi from "joi";
import Handler from "../handler/services.handler";

const routes=[
    {
        path:'/get',
        method:'GET',
        handler:Handler.getall
    },
    {
        path:'/add',
        method:'POST',
        config:{
            validate:{
                payload:joi.object(
                    {
                        name:joi.string().required(),
                        commission:joi.alternatives(
                            joi.string(),
                            joi.number()
                        ).required(),
                        image:joi.string().optional()
                    }
                )
            }
        },
        handler:Handler.add
    },
    {
        path:'/delete/{id}',
        method:'DELETE',
        config:{
            validate:{
                params:joi.object(
                    {
                        id:joi.string().required()
                    }
                )
            }
        },
        handler:Handler.deleteInventory
    }
]

export default routes;