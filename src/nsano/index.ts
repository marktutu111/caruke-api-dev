import { sendRequest, sendSmsRequest } from "./request";
import route from "./route";

const nsano={
    pkg: {
        name:'nsano',
        version: '1'
    },
    register:async(server)=>{
        server.method('nsanoPay',sendRequest);
        server.method('nsanoSendSMS',sendSmsRequest);
        server.route(route)
    }
}

export default nsano;