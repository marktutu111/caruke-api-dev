import { formatePhonenumber } from "../modules/formate-phone-number";

const route=[
    {
        path:'/sendsms',
        method:'POST',
        handler:async(request,h)=>{
            try {
                const {number,message}=request.payload;
                const response=await request.server.methods.nsanoSendSMS(
                    {
                        type:'single',
                        data:{
                            recipient:formatePhonenumber(number),
                            message:message
                        }
                    }
                );
                return h.response(
                    {
                        success:true,
                        message:'success',
                        data:response
                    }
                )
            } catch (err) {
                return h.response(
                    {
                        success:false,
                        message:err.message
                    }
                )
            }
        }
    }
]


export default route;